package ru.t1.dkozoriz.tm.dto.model;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.enumerated.Role;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_session")
public final class SessionDto extends UserOwnedModelDto {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(nullable = false, name = "date")
    private Date date = new Date();

    @Nullable
    @Column(nullable = false, name = "role")
    @Enumerated(EnumType.STRING)
    private Role role = null;

}