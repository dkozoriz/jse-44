package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.task.TaskCreateRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    public TaskCreateCommand() {
        super("task-create", "create new task.");
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        getEndpointLocator().getTaskEndpoint().taskCreate(new TaskCreateRequest(getToken(), name, description));
    }

}
