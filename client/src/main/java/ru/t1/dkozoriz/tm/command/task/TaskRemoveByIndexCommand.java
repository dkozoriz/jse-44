package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.task.TaskRemoveByIndexRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    public TaskRemoveByIndexCommand() {
        super("task-remove-by-index", "remove task by index.");
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        getEndpointLocator().getTaskEndpoint().taskRemoveByIndex(new TaskRemoveByIndexRequest(getToken(), index));
    }

}