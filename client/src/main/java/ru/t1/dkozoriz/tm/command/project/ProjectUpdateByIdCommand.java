package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectUpdateByIdRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    public ProjectUpdateByIdCommand() {
        super("project-update-by-id", "update project by id.");
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        getEndpointLocator().getProjectEndpoint()
                .projectUpdateById(new ProjectUpdateByIdRequest(getToken(), id, name, description));
    }

}