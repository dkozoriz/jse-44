package ru.t1.dkozoriz.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkozoriz.marker.SoapCategory;
import ru.t1.dkozoriz.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dkozoriz.tm.api.endpoint.IUserEndpoint;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;
import ru.t1.dkozoriz.tm.api.service.ITokenService;
import ru.t1.dkozoriz.tm.dto.request.user.*;
import ru.t1.dkozoriz.tm.dto.model.UserDto;
import ru.t1.dkozoriz.tm.service.PropertyService;
import ru.t1.dkozoriz.tm.service.TokenService;


@Category(SoapCategory.class)
public class AuthEndpointTest {

    @NotNull
    private static final ITokenService TOKEN_SERVICE = new TokenService();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IUserEndpoint USER_ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);

    @Before
    public void initTest() {
        @NotNull final String login = "test login";
        @NotNull final String password = "test password";
        @NotNull final String email = "email@test.tu";
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(login, password, email);
        @Nullable final UserDto user = USER_ENDPOINT.userRegistry(request).getUser();
        Assert.assertNotNull(user);
    }

    @Test
    public void testLoginAndLogout() {
        @NotNull final String login = "test login";
        @NotNull final String password = "test password";
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(login, password);
        @Nullable final String userToken = AUTH_ENDPOINT.login(loginRequest).getToken();
        Assert.assertNotNull(userToken);
        @NotNull final UserViewProfileRequest requestView = new UserViewProfileRequest(userToken);
        @NotNull final UserDto currentUser = AUTH_ENDPOINT.getProfile(requestView).getUser();
        Assert.assertNotNull(currentUser);

        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(userToken);
        AUTH_ENDPOINT.logout(logoutRequest);
        @NotNull final UserViewProfileRequest requestViewExc = new UserViewProfileRequest(userToken);
        Assert.assertThrows(Exception.class, () -> AUTH_ENDPOINT.getProfile(requestViewExc));
    }

    @Test
    public void testGetProfile() {
        @NotNull final String login = "test login";
        @NotNull final String password = "test password";
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(login, password);
        @Nullable final String userToken = AUTH_ENDPOINT.login(loginRequest).getToken();
        Assert.assertNotNull(userToken);
        @NotNull final UserViewProfileRequest requestView = new UserViewProfileRequest(userToken);
        @NotNull final UserDto currentUser = AUTH_ENDPOINT.getProfile(requestView).getUser();
        Assert.assertNotNull(currentUser);
    }

    @After
    public void after() {
        @NotNull final String login = "admin";
        @NotNull final String password = "admin";
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(login, password);
        @Nullable final String adminToken = AUTH_ENDPOINT.login(loginRequest).getToken();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken, "test login");
        Assert.assertNotNull(USER_ENDPOINT.userRemove(request));
    }

}