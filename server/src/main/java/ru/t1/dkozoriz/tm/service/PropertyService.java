package ru.t1.dkozoriz.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "src/main/resources/application.properties";

    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "8122";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "546282";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String APPLICATION_FILE_NAME_DEFAULT = "server/src/main/resources/application.properties";

    @NotNull
    private static final String APPLICATION_FILE_NAME_KEY = "application.config";

    @NotNull
    private static final String APPLICATION_NAME_DEFAULT = "tm";

    @NotNull
    private static final String APPLICATION_LOG_KEY = "application.log";

    @NotNull
    private static final String APPLICATION_LOG_DEFAULT = "./";

    @NotNull
    private static final String APPLICATION_NAME_KEY = "application.name";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String GIT_BRANCH = "gitBranch";

    @NotNull
    private static final String GIT_COMMIT_ID = "gitCommitId";

    @NotNull
    private static final String GIT_COMMITTER_NAME = "gitCommitterName";

    @NotNull
    private static final String GIT_COMMITTER_EMAIL = "gitCommitterEmail";

    @NotNull
    private static final String GIT_COMMIT_MESSAGE = "gitCommitMessage";

    @NotNull
    private static final String GIT_COMMIT_TIME = "gitCommitTime";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";;

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String SESSION_KEY = "session.key";;

    @NotNull
    private static final String SESSION_KEY_DEFAULT = "927563801";

    @NotNull
    private static final String SESSION_TIMEOUT = "session.timeout";;

    @NotNull
    private static final String SESSION_TIMEOUT_DEFAULT = "10800";

    @NotNull
    private static final String DB_USERNAME_KEY = "database.username";

    @NotNull
    private static final String DB_PASSWORD_KEY = "database.password";

    @NotNull
    private static final String DB_URL_KEY = "database.url";

    @NotNull
    private static final String DB_DRIVER_KEY = "database.driver";

    @NotNull
    private static final String DATABASE_DIALECT_KEY = "database.dialect";

    @NotNull
    private static final String DATABASE_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQLDialect";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_KEY = "database.hbm2ddl_auto";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_DEFAULT = "update";

    @NotNull
    private static final String DATABASE_SHOW_SQL_KEY = "database.show_sql";

    @NotNull
    private static final String DATABASE_SHOW_SQL_DEFAULT = "true";

    @NotNull
    private static final String PARAMETER_CACHE = "parameter.cache";

    @NotNull
    private static final String PARAMETER_CACHE_DEFAULT = "false";

    @NotNull
    public static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final boolean existConfig = isExistExternalConfig();
        if (existConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_KEY;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @SneakyThrows
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @Nullable final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    private boolean isExistExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @Override
    @NotNull
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }


    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @Override
    @NotNull
    public String getApplicationName() {
        return getStringValue(APPLICATION_NAME_KEY, APPLICATION_NAME_DEFAULT);
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @Override
    @NotNull
    public String getApplicationLog() {
        return getStringValue(APPLICATION_LOG_KEY, APPLICATION_LOG_DEFAULT);
    }

    @Override
    @NotNull
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getGitBranch() {
        return read(GIT_BRANCH);
    }

    @Override
    @NotNull
    public String getGitCommitId() {
        return read(GIT_COMMIT_ID);
    }

    @Override
    @NotNull
    public String getGitCommitterName() {
        return read(GIT_COMMITTER_NAME);
    }

    @Override
    @NotNull
    public String getGitCommitterEmail() {
        return read(GIT_COMMITTER_EMAIL);
    }

    @Override
    @NotNull
    public String getGitCommitMessage() {
        return read(GIT_COMMIT_MESSAGE);
    }

    @Override
    @NotNull
    public String getGitCommitTime() {
        return read(GIT_COMMIT_TIME);
    }

    @Override
    @NotNull
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    @NotNull
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerPort() {
        return getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT, SESSION_TIMEOUT_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBUsername() {
        return getStringValue(DB_USERNAME_KEY);
    }

    @NotNull
    @Override
    public String getDBPassword() {
        return getStringValue(DB_PASSWORD_KEY);
    }

    @NotNull
    @Override
    public String getDBUrl() {
        return getStringValue(DB_URL_KEY);
    }

    @NotNull
    public String getDBDriver() {
        return getStringValue(DB_DRIVER_KEY);
    }

    @NotNull
    @Override
    public String getDBDialect() {
        return getStringValue(DATABASE_DIALECT_KEY, DATABASE_DIALECT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBHmb2DDLAuto() {
        return getStringValue(DATABASE_HBM2DDL_AUTO_KEY, DATABASE_HBM2DDL_AUTO_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBShowSQL() {
        return getStringValue(DATABASE_SHOW_SQL_KEY, DATABASE_SHOW_SQL_DEFAULT);
    }

    @NotNull
    public Boolean getParameterCache() {
        return getBooleanValue(PARAMETER_CACHE, PARAMETER_CACHE_DEFAULT);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private Boolean getBooleanValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Boolean.parseBoolean(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return properties.getProperty(key, EMPTY_VALUE);
    }

}