package ru.t1.dkozoriz.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.model.IAbstractRepository;
import ru.t1.dkozoriz.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public abstract class AbstractRepository<T extends AbstractModel> implements IAbstractRepository<T> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    protected abstract Class<T> getClazz();

    @Override
    public void add(@NotNull final T model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final T model) {
        entityManager.merge(model);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM " + getClazz().getSimpleName() + " m";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    @Nullable
    public List<T> findAll() {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m";
        return entityManager.createQuery(jpql, getClazz()).getResultList();
    }

    @Override
    @Nullable
    public T findById(@NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.id = :id";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream().findAny().orElse(null);
    }

    @Override
    @Nullable
    public T findByIndex(@NotNull final Integer index) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m";
        return entityManager.createQuery(jpql, getClazz())
                .setFirstResult(1)
                .setMaxResults(1)
                .getResultList()
                .stream().findAny().orElse(null);
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getClazz().getSimpleName() + " m";
        return entityManager.createQuery(jpql, Integer.class).getSingleResult();
    }

    @Override
    public void remove(@Nullable final T model) {
        entityManager.remove(model);
    }

    @Override
    public void removeAll(@NotNull final Collection<T> models) {
        for (@NotNull final T model : models) remove(model);
    }

    @Override
    public void addAll(@NotNull Collection<T> models) {
        for (@NotNull final T model : models) add(model);
    }

    @Override
    public void set(@NotNull final Collection<T> models) {
        clear();
        models.forEach(this::add);
    }

}