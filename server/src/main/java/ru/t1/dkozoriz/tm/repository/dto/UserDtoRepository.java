package ru.t1.dkozoriz.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.dkozoriz.tm.dto.model.UserDto;

import javax.persistence.EntityManager;

public final class UserDtoRepository extends AbstractDtoRepository<UserDto> implements IUserDtoRepository {

    public UserDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    protected Class<UserDto> getClazz() {
        return UserDto.class;
    }

    @Override
    @Nullable
    public UserDto findByLogin(@Nullable final String login) {
        @NotNull final String jpql = "SELECT m FROM UserDto m WHERE m.login = :login";
        return entityManager.createQuery(jpql, UserDto.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream().findAny().orElse(null);
    }

    @Override
    @Nullable
    public UserDto findByEmail(@Nullable final String email) {
        @NotNull final String jpql = "SELECT m FROM UserDto m WHERE m.email = :email";
        return entityManager.createQuery(jpql, UserDto.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream().findAny().orElse(null);
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        return findAll()
                .stream()
                .anyMatch(u -> login.equals(u.getLogin()));
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        return findAll()
                .stream()
                .anyMatch(u -> email.equals(u.getEmail()));
    }

}