package ru.t1.dkozoriz.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.model.AbstractModelDto;

import java.util.List;

public interface IAbstractDtoService<T extends AbstractModelDto> {

    @Nullable
    T add(T model);

    void update(T model);

    void clear();

    @NotNull
    List<T> findAll() throws Exception;

    void remove(T model);

    void removeById(@Nullable String id);

    void removeByIndex(@Nullable Integer index);

    @Nullable
    T findById(@Nullable String id);

    @Nullable
    T findByIndex(@Nullable Integer index);

    int getSize() throws Exception;

}
