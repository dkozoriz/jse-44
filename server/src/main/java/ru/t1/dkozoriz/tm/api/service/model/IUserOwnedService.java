package ru.t1.dkozoriz.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.UserOwnedModel;

import java.util.List;

public interface IUserOwnedService<T extends UserOwnedModel> extends IAbstractService<T> {

    T add(@NotNull String userId, T model);

    void update(@NotNull String userId, T model);

    void clear(@NotNull String userId);

    @NotNull List<T> findAll(@NotNull String userId);

    void remove(@NotNull String userId, T model);

    void removeById(@NotNull String userId, @Nullable String id);

    void removeByIndex(@NotNull String userId, @Nullable Integer index);

    T findById(@NotNull String userId, @Nullable String id);

    T findByIndex(@NotNull String userId, @Nullable Integer index);

    int getSize(@NotNull String userId);

}