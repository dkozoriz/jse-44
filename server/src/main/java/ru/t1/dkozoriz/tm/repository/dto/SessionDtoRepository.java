package ru.t1.dkozoriz.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.dkozoriz.tm.dto.model.SessionDto;
import ru.t1.dkozoriz.tm.dto.model.UserDto;

import javax.persistence.EntityManager;

public final class SessionDtoRepository extends UserOwnedDtoRepository<SessionDto> implements ISessionDtoRepository {

    public SessionDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    protected Class<SessionDto> getClazz() {
        return SessionDto.class;
    }

}