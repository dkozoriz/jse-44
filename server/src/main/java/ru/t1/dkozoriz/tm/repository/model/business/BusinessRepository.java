package ru.t1.dkozoriz.tm.repository.model.business;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.repository.dto.business.IBusinessDtoRepository;
import ru.t1.dkozoriz.tm.dto.model.business.BusinessModelDto;
import ru.t1.dkozoriz.tm.model.business.BusinessModel;
import ru.t1.dkozoriz.tm.repository.model.UserOwnedRepository;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class BusinessRepository<T extends BusinessModel> extends UserOwnedRepository<T> implements ru.t1.dkozoriz.tm.api.repository.model.IBusinessRepository<T> {

    protected BusinessRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    public List<T> findAllOrderByName(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId ORDER BY m.name";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).getResultList();
    }

    @Override
    @NotNull
    public List<T> findAllOrderByStatus(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId ORDER BY m.status";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).getResultList();
    }

    @Override
    @NotNull
    public List<T> findAllOrderByCreated(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId ORDER BY m.created";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).getResultList();
    }

}