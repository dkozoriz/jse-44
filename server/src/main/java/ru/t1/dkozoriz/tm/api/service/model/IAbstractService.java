package ru.t1.dkozoriz.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.AbstractModel;

import java.util.List;

public interface IAbstractService<T extends AbstractModel> {

    T add(T model);

    void update(T model);

    void clear();

    @NotNull List<T> findAll() throws Exception;

    void remove(T model);

    void removeById(@Nullable String id);

    void removeByIndex(@Nullable Integer index);

    T findById(@Nullable String id);

    T findByIndex(@Nullable Integer index);

    int getSize();

}