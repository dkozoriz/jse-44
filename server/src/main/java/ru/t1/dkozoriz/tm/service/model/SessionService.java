package ru.t1.dkozoriz.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.repository.model.ISessionRepository;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.model.ISessionService;
import ru.t1.dkozoriz.tm.model.Session;
import ru.t1.dkozoriz.tm.repository.model.SessionRepository;


import javax.persistence.EntityManager;

public final class SessionService extends UserOwnedService<Session> implements ISessionService {

    private final static String NAME = "Session";

    @NotNull
    public String getName() {
        return NAME;
    }

    public SessionService(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    protected ISessionRepository getRepository(@NotNull EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

}