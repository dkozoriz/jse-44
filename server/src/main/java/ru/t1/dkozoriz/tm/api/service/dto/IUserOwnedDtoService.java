package ru.t1.dkozoriz.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.model.UserOwnedModelDto;

import java.util.List;

public interface IUserOwnedDtoService<T extends UserOwnedModelDto> extends IAbstractDtoService<T> {

    @Nullable
    T add(@NotNull String userId, T model);

    void update(@NotNull String userId, T model);

    void clear(@NotNull String userId);

    @NotNull
    List<T> findAll(@NotNull String userId);

    void remove(@NotNull String userId, T model);

    void removeById(@NotNull String userId, @Nullable String id);

    void removeByIndex(@NotNull String userId, @Nullable Integer index);

    @Nullable
    T findById(@NotNull String userId, @Nullable String id);

    @Nullable
    T findByIndex(@NotNull String userId, @Nullable Integer index);

    int getSize(@NotNull String userId);
}
